<?php

declare(strict_types=1);

namespace Drupal\Tests\facets_form_date_range_extended\FunctionalJavascript;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\facets\Functional\BlockTestTrait;
use Drupal\Tests\facets_form\Traits\FacetUrlTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\facets\Entity\Facet;
use Drupal\facets_summary\Entity\FacetsSummary;
use Drupal\search_api\Entity\Index;

/**
 * Tests the Facets Form Date Range Extended Javascript functionality.
 *
 * @group facets_form
 */
class DateRangeExtendedIntegrationTest extends WebDriverTestBase {

  use BlockTestTrait;
  use ContentTypeCreationTrait;
  use FacetUrlTestTrait;
  use NodeCreationTrait;

  /**
   * Nodes created for testing.
   *
   * @var array
   */
  protected $nodes = [];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'facets_summary',
    'facets_form_date_range_extended',
    'facets_form_search_api_dependency',
    'node',
    'search_api_test_db',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Get Facet ID.
   *
   * @return string
   *   Facet ID.
   */
  protected function getFacetId(): string {
    return 'facets_form_date_range_extended';
  }

  /**
   * Get Facet name.
   *
   * @return string
   *   Facet name.
   */
  protected function getFacetName(): string {
    return 'Date Range Extended';
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'bundle_1', 'name' => 'Bundle 1']);
    $this->createContentType(['type' => 'bundle_2', 'name' => 'Bundle 2']);

    $this->installSummaryBlock();
    $this->installDateRangeExtendedFacet();
  }

  /**
   * Generate a set of content nodes for testing.
   */
  protected function generateTestContentSet(string $period = 'week'): void {
    $time_strings = [
      'week' => [
        'tuesday this week midnight',
        'wednesday this week midnight',
        'thursday this week midnight',
        'tuesday last week midnight',
        'thursday last week midnight',
      ],
      'month' => [
        'first monday of this month midnight',
        'last monday of this month midnight',
        'last friday of this month midnight',
        'first monday of last month midnight',
        'last monday of last month midnight',
      ],
    ];

    $test_content_set = [
      [
        'title' => 'Llama (1 - ' . $period . ')',
        'type' => 'bundle_1',
        'created' => (new DrupalDateTime($time_strings[$period][0]))->getTimestamp(),
      ],
      [
        'title' => 'Llama (2 - ' . $period . ')',
        'type' => 'bundle_1',
        'created' => (new DrupalDateTime($time_strings[$period][1]))->getTimestamp(),
      ],
      [
        'title' => 'Llama (3 - ' . $period . ')',
        'type' => 'bundle_1',
        'created' => (new DrupalDateTime($time_strings[$period][2]))->getTimestamp(),
      ],
      [
        'title' => 'Emu - (1 - ' . $period . ')',
        'type' => 'bundle_2',
        'created' => (new DrupalDateTime($time_strings[$period][3]))->getTimestamp(),
      ],
      [
        'title' => 'Emu (2 - ' . $period . ')',
        'type' => 'bundle_2',
        'created' => (new DrupalDateTime($time_strings[$period][4]))->getTimestamp(),
      ],
    ];

    foreach ($test_content_set as $content) {
      $this->nodes[] = $this->createNode($content);
    }

    $this->assertIndexedElementsCount($this->getTestContentCount());
  }

  /**
   * Get total count of elements.
   *
   * @return int
   *   Number of elements.
   */
  protected function getTestContentCount(): int {
    return count($this->nodes);
  }

  /**
   * Create a new facet and place it in search test page.
   */
  protected function installDateRangeExtendedFacet(): void {
    $facet_id = $this->getFacetId();
    $this->createFacet($this->getFacetName(), $facet_id, 'created', 'page_1', 'views_page__test', FALSE);

    $facet = Facet::load($facet_id);
    $facet->setOnlyVisibleWhenFacetSourceIsVisible(FALSE);
    $facet->setWidget($facet_id, [
      'extended_filters' => [
        'extended_filter_this_week' => TRUE,
        'extended_filter_this_month' => TRUE,
        'extended_filter_last_week' => TRUE,
        'extended_filter_last_month' => TRUE,
      ],
    ]);
    $facet->save();

    $this->drupalPlaceBlock('facets_form:search_api:views_page__test__page_1');
  }

  /**
   * Create a new summary block and place it in search test page.
   */
  protected function installSummaryBlock(): void {
    FacetsSummary::create([
      'name' => 'Facets Summary',
      'id' => 'facets_summary_test',
      'facet_source_id' => 'search_api:views_page__test__page_1',
      'facets' => [
        'authored_on' => [
          'checked' => TRUE,
          'show_count' => FALSE,
        ],
      ],
      'processor_configs' => [
        'show_count' => [
          'processor_id' => 'show_count',
        ],
      ],
    ])->save();

    $this->drupalPlaceBlock('facets_summary_block:facets_summary_test');
  }

  /**
   * Gets date pattern to be used in widget.
   *
   * @return string
   *   Date format pattern.
   */
  private function getDatePattern(): string {
    $format_storage = $this->container->get('entity_type.manager')->getStorage('date_format');
    $date_format = $format_storage->load('html_date');

    if ($date_format) {
      return $date_format->getPattern();
    }

    return 'Y-m-d';
  }

  /**
   * Asserts the facets summary result.
   *
   * @param int $expected_count
   *   The number of expected results.
   */
  protected function assertFacetsSummary(int $expected_count): void {
    $assert = $this->assertSession();

    $summary_count = \Drupal::translation()->formatPlural($expected_count, '1 result found', '@count results found');
    $assert->elementTextContains('css', '.source-summary-count', (string) $summary_count);
  }

  /**
   * Asserts the indexed elements count.
   *
   * @param int $expected_count
   *   The number of expected results.
   */
  protected function assertIndexedElementsCount(int $expected_count = 0): void {
    $index = Index::load('test');

    $this->assertSame($expected_count, $index->indexItems());
  }

  /**
   * Tests the behavior of quick pickers - this and last week.
   */
  public function testWeekPickersInDateRangeWidget(): void {
    $assert = $this->assertSession();
    $date_pattern = $this->getDatePattern();
    $facet_id = $this->getFacetId();

    $this->generateTestContentSet('week');

    $this->drupalGet('test');

    $form = $assert->elementExists('css', 'form#facets-form');

    $extended_field_selector = 'input[name="' . $facet_id . '[extended_filters]"]';

    $start_date_selector = 'input[name="' . $facet_id . '[from][date]"]';
    $assert->elementExists('css', $start_date_selector, $form);

    $end_date_selector = 'input[name="' . $facet_id . '[to][date]"]';
    $assert->elementExists('css', $end_date_selector, $form);

    $this_week_selector = $extended_field_selector . '[value="this-week"]';
    $this_week_field = $assert->elementExists('css', $this_week_selector, $form);

    $this_week_field->click();
    $assert->buttonExists('Search', $form)->press();

    $start_date = (new DrupalDateTime('monday this week midnight'))->format($date_pattern);
    $end_date = (new DrupalDateTime('sunday this week midnight'))->format($date_pattern);
    $this->assertCurrentUrl('test', ['f' => [$facet_id . ':' . $start_date . '~' . $end_date]]);

    $this->assertFacetsSummary(3);

    $last_week_selector = $extended_field_selector . '[value="last-week"]';
    $last_week_field = $assert->elementExists('css', $last_week_selector, $form);

    $last_week_field->click();
    $assert->buttonExists('Search', $form)->press();

    $start_date = (new DrupalDateTime('monday last week midnight'))->format($date_pattern);
    $end_date = (new DrupalDateTime('sunday last week midnight'))->format($date_pattern);
    $this->assertCurrentUrl('test', ['f' => [$facet_id . ':' . $start_date . '~' . $end_date]]);

    $this->assertFacetsSummary(2);
  }

  /**
   * Tests the behavior of quick pickers - this and last month.
   */
  public function testMonthPickersInDateRangeWidget(): void {
    $assert = $this->assertSession();
    $date_pattern = $this->getDatePattern();
    $facet_id = $this->getFacetId();

    $this->generateTestContentSet('month');

    $this->drupalGet('test');

    $form = $assert->elementExists('css', 'form#facets-form');

    $extended_field_selector = 'input[name="' . $facet_id . '[extended_filters]"]';

    $start_date_selector = 'input[name="' . $facet_id . '[from][date]"]';
    $assert->elementExists('css', $start_date_selector, $form);

    $end_date_selector = 'input[name="' . $facet_id . '[to][date]"]';
    $assert->elementExists('css', $end_date_selector, $form);

    $this_month_selector = $extended_field_selector . '[value="this-month"]';
    $this_month_field = $assert->elementExists('css', $this_month_selector, $form);

    $this_month_field->click();
    $assert->buttonExists('Search', $form)->press();

    $start_date = (new DrupalDateTime('first day of this month midnight'))->format($date_pattern);
    $end_date = (new DrupalDateTime('last day of this month midnight'))->format($date_pattern);
    $this->assertCurrentUrl('test', ['f' => [$facet_id . ':' . $start_date . '~' . $end_date]]);

    $this->assertFacetsSummary(3);

    $last_month_selector = $extended_field_selector . '[value="last-month"]';
    $last_month_field = $assert->elementExists('css', $last_month_selector, $form);

    $last_month_field->click();
    $assert->buttonExists('Search', $form)->press();

    $start_date = (new DrupalDateTime('first day of last month midnight'))->format($date_pattern);
    $end_date = (new DrupalDateTime('last day of last month midnight'))->format($date_pattern);
    $this->assertCurrentUrl('test', ['f' => [$facet_id . ':' . $start_date . '~' . $end_date]]);

    $this->assertFacetsSummary(2);
  }

}
