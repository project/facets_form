<?php

declare(strict_types=1);

namespace Drupal\facets_form_date_range_extended\Plugin\facets\widget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\facets\FacetInterface;
use Drupal\facets_form_date_range\DateRange;
use Drupal\facets_form_date_range\Plugin\facets\widget\DateRangeWidget;

/**
 * Date range widget with extended filters.
 *
 * @FacetsWidget(
 *   id = "facets_form_date_range_extended",
 *   label = @Translation("Date range with quick filters (inside form)"),
 *   description = @Translation("A configurable widget that shows a date range
 *   as a form element."),
 * )
 */
class DateRangeExtendedWidget extends DateRangeWidget {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'extended_filters' => [
        'extended_filter_this_week' => FALSE,
        'extended_filter_this_month' => FALSE,
        'extended_filter_last_week' => FALSE,
        'extended_filter_last_month' => FALSE,
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $config_form = parent::buildConfigurationForm($form, $form_state, $facet);

    $configuration = $this->getConfiguration();
    $extended_filters = $configuration['extended_filters'] ?? [];

    $config_form['extended_filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Enable quick filters'),
      'extended_filter_this_week' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable filter for <strong>this week</strong>.'),
        '#default_value' => $extended_filters['extended_filter_this_week'] ?? FALSE,
      ],
      'extended_filter_this_month' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable filter for <strong>this month</strong>.'),
        '#default_value' => $extended_filters['extended_filter_this_month'] ?? FALSE,
      ],
      'extended_filter_last_week' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable filter for <strong>last week</strong>.'),
        '#default_value' => $extended_filters['extended_filter_last_week'] ?? FALSE,
      ],
      'extended_filter_last_month' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable filter for <strong>last month</strong>.'),
        '#default_value' => $extended_filters['extended_filter_last_month'] ?? FALSE,
      ],
    ];

    return $config_form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet): array {
    $facet_form = parent::build($facet);

    if (empty($facet_form)) {
      return [];
    }

    $config = $this->getConfiguration();

    if (!empty($config['extended_filters'])) {
      $extended_filters = $this->getExtendedFiltersSettings($config['extended_filters']);
      if (!empty($extended_filters)) {
        $this->buildExtendedFilters($facet, $facet_form, $extended_filters);

        $facet_form['#attached']['library'][] = 'facets_form_date_range_extended/extended-filters';
      }
    }

    return $facet_form;
  }

  /**
   * Adds checkboxes for extended filters to facet.
   *
   * @param \Drupal\facets\FacetInterface $facet
   *   Facet object.
   * @param array $form
   *   Facet form to be rendered.
   * @param array $extended_filters_settings
   *   Settings for Extended filters.
   */
  protected function buildExtendedFilters(FacetInterface $facet, array &$form, array $extended_filters_settings): void {
    $facet_id = $facet->id();

    $date_range = DateRange::createFromFacet($facet);
    $date_format_pattern = $this->getDatePattern();

    $from = $date_range->getFromDateAsDatetime();
    $from_date = (!empty($from) ? $from->format($date_format_pattern) : NULL);
    $to = $date_range->getToDateAsDatetime();
    $to_date = (!empty($to) ? $to->format($date_format_pattern) : NULL);

    $form[$facet_id]['extended_filters'] = [
      '#type' => 'radios',
      '#wrapper_attributes' => [
        'class' => [
          'ff-date-range-filters-list',
        ],
      ],
      '#options' => [],
    ];

    foreach ($extended_filters_settings as $filter_id => $filter) {
      $form[$facet_id]['extended_filters']['#options'][$filter_id] = $filter['label'];
      $form[$facet_id]['extended_filters'][$filter_id]['#wrapper_attributes'] = [
        'class' => [
          'ff-date-range-filters-row',
        ],
      ];
      $form[$facet_id]['extended_filters'][$filter_id]['#attributes'] = [
        'class' => [
          'ff-date-range-filter-item',
        ],
        'data-date-range-min' => $filter['min'],
        'data-date-range-max' => $filter['max'],
      ];

      if (!empty($from_date) && $from_date === $filter['min'] &&
          !empty($to_date) && $to_date === $filter['max']) {
        $form[$facet_id]['extended_filters']['#default_value'] = $filter_id;
      }
    }
  }

  /**
   * Adds widget configuration for Extended filters.
   *
   * @param array $filters
   *   Array of extended_filters to be enabled.
   *
   * @return array
   *   Settings for extended filters to be created by library.
   */
  protected function getExtendedFiltersSettings(array $filters): array {
    $date_format_pattern = $this->getDatePattern();
    $extended_filters = [];

    // "This week" filter.
    if (!empty($filters['extended_filter_this_week'])) {
      $this_monday = new DrupalDateTime('monday this week midnight');
      $this_sunday = new DrupalDateTime('sunday this week midnight');

      $extended_filters['this-week'] = [
        'label' => $this->t('This week'),
        'min' => $this_monday->format($date_format_pattern),
        'max' => $this_sunday->format($date_format_pattern),
      ];
    }

    // "This month" filter.
    if (!empty($filters['extended_filter_this_month'])) {
      $this_month_first = new DrupalDateTime('first day of this month midnight');
      $this_month_last = new DrupalDateTime('last day of this month midnight');
      $extended_filters['this-month'] = [
        'label' => $this->t('This month'),
        'min' => $this_month_first->format($date_format_pattern),
        'max' => $this_month_last->format($date_format_pattern),
      ];
    }

    // "Last week" filter.
    if (!empty($filters['extended_filter_last_week'])) {
      $last_monday = new DrupalDateTime('monday last week midnight');
      $last_sunday = new DrupalDateTime('sunday last week midnight');
      $extended_filters['last-week'] = [
        'label' => $this->t('Last week'),
        'min' => $last_monday->format($date_format_pattern),
        'max' => $last_sunday->format($date_format_pattern),
      ];
    }

    // "Last month" filter.
    if (!empty($filters['extended_filter_last_month'])) {
      $last_month_first = new DrupalDateTime('first day of last month midnight');
      $last_month_last = new DrupalDateTime('last day of last month midnight');
      $extended_filters['last-month'] = [
        'label' => $this->t('Last month'),
        'min' => $last_month_first->format($date_format_pattern),
        'max' => $last_month_last->format($date_format_pattern),
      ];
    }

    return $extended_filters;
  }

  /**
   * Gets a date pattern to be used in the widget.
   *
   * @return string
   *   Date format pattern.
   */
  private function getDatePattern(): string {
    $format_storage = $this->entityTypeManager->getStorage('date_format');

    $date_format = $format_storage->load('html_date');

    return $date_format->getPattern();
  }

}
