/**
 * @file
 * Facets Form Date Range Extended: Add quick pickers to Date range facet with
 * predefined dates (This week, This month, Last week, Last month).
 */

(function (Drupal, once) {

  /**
   * Implement quick pickers for predefined dates in Date range facet.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the extended filters.
   */
  Drupal.behaviors.dateRangeExtendedFilters = {
    attach(context) {
      once('DateRangeExtendedFilters', '[data-drupal-facets-form-widget="facets_form_date_range_extended"]', context).forEach(facet => {
        const filters_selector = '.ff-date-range-filters-row .ff-date-range-filter-item';

        /**
         * Replace values in facet form.
         *
         * @param facet
         *   Facet item.
         * @param min
         *   Minimum value.
         * @param max
         *   Maximum value.
         */
        const applyFilters = (facet, min, max) => {
          const facet_id = facet.getAttribute('data-drupal-selector'),
                from_input = facet.querySelector('#' + facet_id + '-from-date'),
                to_input = facet.querySelector('#' + facet_id + '-to-date')

          from_input.value = min;
          to_input.value = max;
        };

        /**
         * Sets quick pickers as active depending on current date range.
         *
         * @param facet
         *   Facet item.
         */
        const setActiveFilter = (facet) => {
          const filters = facet.querySelectorAll(filters_selector),
                facet_id = facet.getAttribute('data-drupal-selector'),
                min_value = facet.querySelector('#' + facet_id + '-from-date').value,
                max_value = facet.querySelector('#' + facet_id + '-to-date').value;

          if (min_value && max_value) {
            filters.forEach(filter_item => {
              const min = filter_item.getAttribute('data-date-range-min'),
                    max = filter_item.getAttribute('data-date-range-max');

              filter_item.checked = (min === min_value && max === max_value);
            });
          }
        };

        /**
         * Manages events applied to Date fields in widget.
         *
         * @param facet
         *   Facet item.
         */
        const addEventsToDateFields = (facet) => {
          const facet_id = facet.getAttribute('data-drupal-selector'),
                from_input = facet.querySelector('#' + facet_id + '-from-date'),
                to_input = facet.querySelector('#' + facet_id + '-to-date'),
                inputs = [from_input, to_input],
                events = ['change', 'blur'];

          inputs.forEach((input) => {
            events.forEach((action) => {
              input.addEventListener(action, (ev) => {
                setActiveFilter(facet);
              });
            });
          });
        };

        /**
         * Manages events applied to Extended filters.
         *
         * @param facet
         *   Facet item.
         */
        const addEventsToFilters = (facet) => {
          const filter_items = facet.querySelectorAll(filters_selector);

          if (!filter_items.length) {
            return;
          }

          filter_items.forEach(filter_item => {
            filter_item.addEventListener('change', (ev) => {
              const filter = ev.currentTarget,
                    wrapper = filter.closest('[data-drupal-facets-form-widget="facets_form_date_range_extended"]');
              let min_value,
                  max_value;

              if (!filter.checked) {
                console.info('Date range extended facet: Change event fired but checkbox is not checked.');
                return;
              }
              if (!wrapper) {
                console.error("Date range extended facet: Facet wrapper not found.");
                return;
              }

              min_value = filter.getAttribute('data-date-range-min');
              max_value = filter.getAttribute('data-date-range-max');

              applyFilters(wrapper, min_value, max_value);
            });
          });
        };

        setActiveFilter(facet);
        addEventsToDateFields(facet);
        addEventsToFilters(facet);
      });
    },
  };

})(Drupal, once);
